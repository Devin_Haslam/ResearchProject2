#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <iomanip>   // format manipulation
#include <direct.h>
#include <string>     // std::string, std::stoi
#include <conio.h>


#include "include/protein.h"
#include "include/skeleton_overall.h"
#include "include/MRC.h"
#include "include/axis.h"

#define SSTR( x ) dynamic_cast< std::ostringstream & >(( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

int main(int argc, char *argv[])  {
	Protein pdb;		//protein structure in xyz coordinate
	Protein pdb2;
	string oFile, path, pdbID, helixName, pdbFileName, mrcFileName, pdbOutName, mrcOutName, chainID, option, outputFilename, id_str, helBaseName, helEndName, shtBaseName, shtEndName, tempera, tempera2;
    string pathFolder = "";
	int i, j, just_a_number_holder;
    float rise = 1.5;					//the rise of the axis
	short radius = 6;
	double sradius = 2.5;
	int currHel = -1;
	int currSht = -1;
	int offset2 = 0;
    bool one = false;
    bool one2 = false;
    bool can = false;
    bool outExist = false;
    bool noHelix = false;
    bool noSheet = false;
    bool touched = false;

    if(argc == 5)
    {
        pdbFileName = argv[1];
        pdbFileName = pdbFileName + ".pdb";
        path = pdbFileName.substr(0, pdbFileName.length()-8);
        pdbID = pdbFileName.substr(pdbFileName.length()-8, 4);
        tempera = argv[2];
        if(tempera != "Empty")
        {
            for(int y = 0; y < tempera.length(); y++)
            {
                if(tempera.substr(tempera.length()-y, 1) == "0")
                {
                    currHel = 0;
                    helBaseName = tempera.substr(0, tempera.length()-y);
                    helEndName = tempera.substr(tempera.length()-y+1,tempera.length()-(tempera.length()-y+1));
                    y = tempera.length();
                }
                if(tempera.substr(tempera.length()-y, 1) == "1")
                {
                    currHel = 1;
                    helBaseName = tempera.substr(0, tempera.length()-y);
                    helEndName = tempera.substr(tempera.length()-y+1,tempera.length()-(tempera.length()-y+1));
                    y = tempera.length();
                    one = true;
                }
            }
        }
        else
        {
            noHelix = true;
        }
        tempera2 = argv[3];
        if(tempera2 != "Empty")
        {

            string fileName = tempera2.c_str();
            fileName = fileName + ".pdb";
            ifstream cFile;
            cFile.open(fileName.c_str());
            pdb2.read(fileName.c_str());
            size_t lines_count2 =0;
            string templine3;
            while (std::getline(cFile , templine3))
            {
                if(templine3.substr(0,4) == "ATOM")
                    ++lines_count2;
            }

            cFile.close();
            int vs = 0;
            double total2 = 0;
            Coordinate last2;
            Coordinate x;
            int temp2 = 0;
            bool OneFile = true;
            while(vs < lines_count2-1)
            {
                if(vs != 0)
                {
                    last2 = x;
                    x = pdb2.AAs[0].atoms[vs].coord;
                    total2 = sqrt((last2.x-x.x)*(last2.x-x.x) + (last2.y-x.y)*(last2.y-x.y) + (last2.z-x.z)*(last2.z-x.z));
                    if(total2 > 1.5)
                    {
                        OneFile = false;
                        vs = lines_count2;
                    }
                }
                else
                    x = pdb2.AAs[0].atoms[vs].coord;
                vs++;
            }
            if(OneFile == true)
            {
                for(int y = tempera2.length()-4; y >0; y--)
                {
                    if(tempera2.substr(y, 1) == "/")
                    {
                        offset2 = y;
                        break;
                    }
                }
                for(int y = 0; y < tempera2.length()-4-offset2; y++)
                {
                    if(tempera2.substr(tempera2.length()-y, 1) == "0")
                    {
                        currSht = 0;
                        shtBaseName = tempera2.substr(0, tempera2.length()-y);
                        shtEndName = tempera2.substr(tempera2.length()-y+1,tempera2.length()-(tempera2.length()-y+1));
                        y = tempera2.length();
                        touched = true;
                    }
                    if(tempera2.substr(tempera2.length()-y, 1) == "1")
                    {
                        currSht = 1;
                        shtBaseName = tempera2.substr(0, tempera2.length()-y);
                        shtEndName = tempera2.substr(tempera2.length()-y+1,tempera2.length()-(tempera2.length()-y+1));
                        y = tempera2.length();
                        one2 = true;
                        touched = true;
                    }
                }
            }
            //wait for python code to delete old files
            Sleep(1000);
            //handles when input is only one file
            if(touched == false)
            {
                //declaring variables
                ifstream inFile;
                ofstream outFile;
                ofstream outFile2;
                int sheetit = 1;
                int it2 = 1;
                int temp1;
                double tempX = 0;
                double tempY = 0;
                double tempZ = 0;
                string temp2;
                double total = 0;
                int bt = 0;
                bool firstPass = true;
                Coordinate x;
                Coordinate last;
                //opening initial input and output files
                //inFile.open(tempera2.c_str(), ios::in);

                tempera2 = tempera2 + ".pdb";
                pdb2.read(tempera2.c_str());
                x = pdb2.AAs[0].atoms[0].coord;
                string outputFilename = path + "/output/sheet_" + toString(sheetit) + ".pdb";
                outFile.open(outputFilename.c_str(), ios::out);
                //read in values from input file
                //inFile >> temp2 >> temp1 >> temp2 >> temp2 >> temp2 >> temp1 >> valX >> valY >> valZ;
                tempX = x.x;
                tempY = x.y;
                tempZ = x.z;
                if(it2 <= 9)
                    outFile << fixed << setprecision(3) << "ATOM      " << it2 << "  H   HOH A   1      " << tempX << "  " << tempY << "  " << tempZ << endl;
                else
                    outFile << fixed << setprecision(3) << "ATOM     " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;

                //check number of lines in file
                ifstream aFile (tempera2.c_str());
                size_t lines_count =0;
                string line;
                while (std::getline(aFile , line))
                {
                    if(line.substr(0,4) == "ATOM")
                        ++lines_count;
                }

                aFile.close();

                int lineCount[100];
                int strandCount = 0;
                for(int i = 0; i < 100; i++)
                {
                    lineCount[i] = 0;
                }
                ifstream bFile (tempera2.c_str());
                for(int i = 0; i < lines_count; i++)
                {
                    if(i!=0)
                    {
                        last = x;
                        x = pdb2.AAs[0].atoms[i].coord;
                        total = sqrt((last.x-x.x)*(last.x-x.x) + (last.y-x.y)*(last.y-x.y) + (last.z-x.z)*(last.z-x.z));
                    }
                    else
                    {
                        x = pdb2.AAs[0].atoms[i].coord;
                    }
                    if(total > 1.5)
                    {
                        strandCount++;
                        lineCount[strandCount]++;
                    }
                    else
                        lineCount[strandCount]++;
                }
                bFile.close();
                strandCount = 0;
                int currentCompare = 0;
                int div = 0;
                int remainder = 0;
                bool tooManyLines = false;
                //for(int j = 0; j < 10; j++)
                //    cout << lineCount[j] <<  endl;
                //cout << lines_count;
                while(bt <lines_count)
                {

                    if(bt == 0)
                    {
                        if(lineCount[strandCount] > 20)
                        {
                            tooManyLines = true;
                            div = lineCount[strandCount]/20;
                            remainder = lineCount[strandCount]%20;
                            if(remainder != 0)
                            {
                                currentCompare = div+1;
                                remainder--;
                            }
                            else
                                currentCompare = div;
                        }
                        else
                            currentCompare = it2+1;
                    }
                    //cout << it2 << " " << bt << " " << lineCount[strandCount] << "\n";
                    if(it2 == lineCount[strandCount])
                    {
                        //cout << "test" << lineCount[strandCount];
                        strandCount++;
                        if(lineCount[strandCount] > 20)
                        {
                            div = lineCount[strandCount]/20;
                            remainder = lineCount[strandCount]%20;
                            if(remainder != 0)
                            {
                                currentCompare = div+1;
                                remainder--;
                            }
                            else
                                currentCompare = div;
                        }
                        else
                            currentCompare = it2+1;
                    }

                    bt++;
                    it2++;
                    total = 0;

                    //set up total comparison
                    last = x;
                    x = pdb2.AAs[0].atoms[bt].coord;
                    total = sqrt((last.x-x.x)*(last.x-x.x) + (last.y-x.y)*(last.y-x.y) + (last.z-x.z)*(last.z-x.z));
                    //cout << total << endl;
                    if(total > 1.5 && it2 != 2)
                    {
                        sheetit++;
                        it2=1;
                        if(firstPass == true)
                        {
                            outFile.close();
                            firstPass = false;
                        }
                        else
                            outFile2.close();
                        if(bt != lines_count)
                        {
                            //cout << "bt:" << bt <<endl;
                            outputFilename = path + "/output/sheet_" + toString(sheetit) + ".pdb";
                            outFile2.open(outputFilename.c_str(), ios::out);
                            tempX = x.x;
                            tempY = x.y;
                            tempZ = x.z;
                            if(it2 < 10)
                                outFile2 << fixed << setprecision(3) << "ATOM      " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                            else if(it2 > 9 && it2 < 100)
                                outFile2 <<fixed << setprecision(3) << "ATOM     " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                            else
                                outFile2 <<fixed << setprecision(3) << "ATOM    " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                        }
                    }
                    else
                    {
                        if(true)
                        {
                            if(firstPass == true)
                            {

                                tempX = x.x;
                                tempY = x.y;
                                tempZ = x.z;
                                if(it2 < 10)
                                    outFile <<fixed << setprecision(3) << "ATOM      " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                                else if(it2 > 9 && it2 < 100)
                                    outFile <<fixed << setprecision(3) << "ATOM     " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                                else
                                    outFile <<fixed << setprecision(3) << "ATOM    " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                            }
                            else
                            {
                                tempX = x.x;
                                tempY = x.y;
                                tempZ = x.z;
                                if(it2 < 10)
                                    outFile2 <<fixed << setprecision(3) << "ATOM      " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                                else if(it2 > 9 && it2 < 100)
                                    outFile2 <<fixed << setprecision(3) << "ATOM     " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                                else
                                    outFile2 <<fixed << setprecision(3) << "ATOM    " << it2 << "  H   HOH A   1      " << tempX  << "  " << tempY << "  " << tempZ << endl;
                            }

                            if(tooManyLines == false)
                            {
                                cout << it2 << " " << currentCompare << " " << bt << "\n";
                                currentCompare++;
                            }
                            else
                            {
                                if(remainder != 0)
                                {
                                    currentCompare += div+1;
                                    remainder--;
                                }
                                else
                                    currentCompare += div;
                            }

                        }

                    }
                }

                tempera2 = path + "/output/sheet_1";
                for(int y = 0; y < tempera2.length()-4; y++)
                {
                    //cout << tempera2.substr(tempera2.length()-y, 1);
                    if(tempera2.substr(tempera2.length()-y, 1) == "1")
                    {
                        //cout << "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
                        currSht = 1;
                        shtBaseName = tempera2.substr(0, tempera2.length()-y);
                        shtEndName = tempera2.substr(tempera2.length()-y+1,tempera2.length()-(tempera2.length()-y+1));
                        y = tempera2.length();
                        one2 = true;
                    }
                }
                currSht = 1;
                //shtBaseName = path + "SHT";
                //shtEndName = ".pdb";
                one2 = true;
                //cout << "aaaaaaaaa" << shtBaseName << "  " << shtEndName << endl;
                outFile2.close();
            }
            ///end of truing to split files
        }
        else
        {
            noSheet = true;
        }

        oFile = argv[4];
        outExist = true;
        if(oFile == "Empty")
        {
            outExist = false;
        }
    }
    else
    {
        cout<<"usage: "<< argv[0] <<" path "<<" pdbID "<<" threshold "<<endl; //argv[0] is the program name
        exit(1);
    }
    CreateDirectory ("C:\\random", NULL);
    //cout << pdbFileName << " " << path << " " << pdbID << " " << helBaseName << currHel << helEndName << " " << sradius << " " << oFile << " " << outExist;
    pdb.read(pdbFileName.c_str());
    vector<Coordinate> axis;

    ofstream outputFile;
    int number_of_hel;
    double stepSize;
    stepSize = 0.1;

    vector<Axis> helTraceArray;
    vector<Axis> helTrueArray;

    bool shortHelix[pdb.hlces.size()+pdb.sheets.size()];
    for(i = 0; i < pdb.hlces.size()+pdb.sheets.size(); i++)
    {
        shortHelix[i] = false;
    }

/*
    pathFolder = path + pdbID +"Output";
    DWORD dwAttrib = GetFileAttributes(pathFolder.c_str());
    while(dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY))
    {
        if (pathFolder.substr(pathFolder.length() - 1, 1) == ")")
            {
                if (pathFolder.substr(pathFolder.length() - 4, 1) == "(")
                {
                    int temp = -1;
                    bool has_only_digits = (pathFolder.substr(pathFolder.length() - 3, 2).find_first_not_of( "0123456789" ) == string::npos);
                    temp = stoi(pathFolder.substr(pathFolder.length() - 3, 2),nullptr);
                    //int.TryParse(pathFolder.substr(pathFolder.length() - 3, 2), out temp);
                    if (temp != -1)
                    {
                        temp++;
                        pathFolder = pathFolder.substr(0, pathFolder.length() - 4) + "(" + temp + ")";
                    }
                }
                if (pathFolder.substr(pathFolder.length() - 3, 1) == "(")
                {
                    int temp = -1;
                    int.TryParse(pathFolder.substr(pathFolder.length() - 2, 1), out temp);
                    if (temp != -1)
                    {
                        temp++;
                        pathFolder = pathFolder.substr(0, pathFolder.length() - 3) + "(" + temp + ")";
                    }
                }
            }
            dwAttrib = GetFileAttributes(pathFolder.c_str());
    }
*/

    for (i = 0 ; i < pdb.hlces.size(); i++)
    {
        Coordinate pdbEnd1;
        Coordinate pdbEnd2;
        just_a_number_holder = i + 1;
        stringstream ss;
        ss << just_a_number_holder;
        id_str = ss.str();
        ///id_str = SSTR(i);
        /** id_str = static_cast<ostringstream*>( &(ostringstream() << i) )->str(); */

        outputFilename = path + "output/trueHelix" + id_str + ".pdb";
        ss.str( std::string() );
        ss.clear();
        axis.clear();
        pdb.setAxisSegments(pdb.hlces[i].startIndx, pdb.hlces[i].endIndx, rise, axis, shortHelix, i);
        //cerr << "Helix " << i+1 << endl;
        pdbEnd1 = pdb.AAs[pdb.hlces[i].startIndx].atoms[pdb.getAtomIndx(pdb.hlces[i].startIndx, "CA")].coord;
        pdbEnd2 = pdb.AAs[pdb.hlces[i].endIndx].atoms[pdb.getAtomIndx(pdb.hlces[i].endIndx, "CA")].coord;
        Axis myAxis;
        myAxis.axisPoints = axis;
        myAxis.addTrueEnds(pdbEnd1, pdbEnd2);
        myAxis.catmullRom(stepSize);
        myAxis.printAsPnts(outputFilename);
        helTrueArray.push_back(myAxis);

    }
    for (i = 0 ; i < pdb.sheets.size(); i++)
    {
        Coordinate pdbEnd1;
        Coordinate pdbEnd2;
        Coordinate last;
        just_a_number_holder = i + 1 + pdb.hlces.size();
        stringstream ss;
        ss << just_a_number_holder;
        id_str = ss.str();
        ///id_str = SSTR(i);
        /** id_str = static_cast<ostringstream*>( &(ostringstream() << i) )->str(); */

        outputFilename = path + "/output/trueSheet" + id_str + ".pdb";
        ss.str( std::string() );
        ss.clear();
        axis.clear();

        pdbEnd1 = pdb.AAs[pdb.sheets[i].endIndx].atoms[pdb.numOfAtoms(pdb.sheets[i].endIndx)-2].coord;
        pdbEnd2 = pdb.AAs[pdb.sheets[i].endIndx+1].atoms[pdb.getAtomIndx(pdb.sheets[i].endIndx, "N")].coord;
        last.x = (pdbEnd1.x+pdbEnd2.x)/2;
        last.y = (pdbEnd1.y+pdbEnd2.y)/2;
        last.z = (pdbEnd1.z+pdbEnd2.z)/2;
        pdb.setAxisSegments2(pdb.sheets[i].startIndx, pdb.sheets[i].endIndx, rise, axis, last, shortHelix, i+pdb.hlces.size());
        //cout << pdbEnd1.x << " " << pdbEnd1.y << " " << pdbEnd1.z << "\n";
        //cout << pdbEnd1.x << " " << pdbEnd1.y << " " << pdbEnd1.z << "\n\n";
        Axis myAxis;
        myAxis.axisPoints = axis;
        //myAxis.addTrueEnds(pdbEnd1, pdbEnd2);
        myAxis.catmullRom(stepSize);
        myAxis.printAsPnts(outputFilename);
        helTrueArray.push_back(myAxis);

    }

    ifstream inFile_tracer;
    vector<Coordinate> helPoints;

int offset = 0;
        stringstream sstring;
        sstring << currHel;
        string current_helix_str = sstring.str();
        string file_helix_name = helBaseName  + current_helix_str + helEndName + ".pdb";
        inFile_tracer.open(file_helix_name.c_str());
        if(!inFile_tracer)
            {cerr << "Could not open " << file_helix_name << endl;}

    while(inFile_tracer) {
        Coordinate tmppnt;
        string line;
        sstring.str(std::string());
        sstring.clear();
        while(getline(inFile_tracer, line))
        {
            line.erase(0,30);
            sstring << line;
            sstring >> tmppnt.x  >> tmppnt.y >> tmppnt.z;

            //cerr << tmppnt.x << " " << tmppnt.y << " " << tmppnt.z << endl;
            helPoints.push_back(tmppnt);
            sstring.str(std::string());
            sstring.clear();
        }
        Axis myAxis;
        myAxis.axisPoints = helPoints;
        myAxis.catmullRom(stepSize);
        if(one == false)
        {
            if (currHel <= pdb.hlces.size())
            {
                outputFilename = path + "/output/traceHelix" + current_helix_str + ".pdb";
            }
            else
                outputFilename = path + "/output/traceSheet" + current_helix_str + ".pdb";
        }
        if(one == true)
        {
            if (currHel <= pdb.hlces.size()+1)
            {
                outputFilename = path + "/output/traceHelix" + current_helix_str + ".pdb";
            }
            else
                outputFilename = path + "/output/traceSheet" + current_helix_str + ".pdb";
        }

        //cerr << "Now writing to " << outputFilename <<endl;
        myAxis.printAsPnts2(outputFilename);
        helTraceArray.push_back(myAxis);
        helPoints.clear();
        inFile_tracer.close();

        currHel++;
        sstring << currHel;
        current_helix_str = sstring.str();
        file_helix_name = helBaseName  + current_helix_str + helEndName + ".pdb";
        //cout << "Helix #" << current_helix_str << endl << "-----------------" << endl;
        inFile_tracer.open(file_helix_name.c_str());
    }
        offset = currHel-1;
        stringstream s2string;
        s2string << currSht;
        string current_sheet_str = s2string.str();
        string file_sheet_name = shtBaseName  + current_sheet_str + shtEndName + ".pdb";
        //cout << file_sheet_name;
        inFile_tracer.open(file_sheet_name.c_str());
        if(!inFile_tracer)
            {cerr << "Could not open " << file_sheet_name << endl;}

    while(inFile_tracer) {
        Coordinate tmppnt;
        string line;
        s2string.str(std::string());
        s2string.clear();
        while(getline(inFile_tracer, line))
        {
            line.erase(0,30);
            s2string << line;
            s2string >> tmppnt.x  >> tmppnt.y >> tmppnt.z;

            //cerr << tmppnt.x << " " << tmppnt.y << " " << tmppnt.z << endl;
            helPoints.push_back(tmppnt);
            s2string.str(std::string());
            s2string.clear();
        }
        Axis myAxis;
        myAxis.axisPoints = helPoints;
        myAxis.catmullRom(stepSize);

        outputFilename = path + "/output/traceSheet" + current_sheet_str + ".pdb";

        //cerr << "Now writing to " << outputFilename <<endl;
        myAxis.printAsPnts2(outputFilename);
        helTraceArray.push_back(myAxis);
        helPoints.clear();
        inFile_tracer.close();

        currSht++;
        s2string << currSht;
        current_sheet_str = s2string.str();
        file_sheet_name = shtBaseName  + current_sheet_str + shtEndName + ".pdb";

        //cout << "Helix #" << current_helix_str << endl << "-----------------" << endl;
        inFile_tracer.open(file_sheet_name.c_str());
    }
    if(noSheet)
        number_of_hel = currHel-1;
    else
    {
        if(noHelix)
            number_of_hel = currSht-1;
        else
            number_of_hel = currHel-1+currSht-1;
    }

    // END OF INPUT


cerr << "Beginning matching." << endl;

int matchedHels[pdb.hlces.size() + pdb.sheets.size()];
int distanceFromCenter[pdb.hlces.size() + pdb.sheets.size()];
for (int i = 0; i < pdb.hlces.size() + pdb.sheets.size(); i++)
{
    matchedHels[i] = 99999;
    distanceFromCenter[i] = 99999;
}

Displacement Dis;
 double distCent;
 Coordinate trueCenter;
 Coordinate traceCenter;
 double minDist;
 for (int currTrueHel = 0; currTrueHel < pdb.hlces.size() + pdb.sheets.size(); currTrueHel++) //for each true helix position
 {
    trueCenter.x = (helTrueArray[currTrueHel].axisPoints[0].x + helTrueArray[currTrueHel].axisPoints[helTrueArray[currTrueHel].axisPoints.size()-1].x)/2;
    trueCenter.y = (helTrueArray[currTrueHel].axisPoints[0].y + helTrueArray[currTrueHel].axisPoints[helTrueArray[currTrueHel].axisPoints.size()-1].y)/2;
    trueCenter.z = (helTrueArray[currTrueHel].axisPoints[0].z + helTrueArray[currTrueHel].axisPoints[helTrueArray[currTrueHel].axisPoints.size()-1].z)/2;
    //cerr <<endl << "True center: " << trueCenter.x << " " << trueCenter.y << " " << trueCenter.z << endl;

     minDist = 5.0;
     for (int currTraceHel = 0; currTraceHel < number_of_hel; currTraceHel++) // check each trace for nearness
     {
         //cerr << "Comparing trace " << currTraceHel << endl;
         traceCenter.x = (helTraceArray[currTraceHel].axisPoints[0].x + helTraceArray[currTraceHel].axisPoints[helTraceArray[currTraceHel].axisPoints.size()-1].x)/2;
         traceCenter.y = (helTraceArray[currTraceHel].axisPoints[0].y + helTraceArray[currTraceHel].axisPoints[helTraceArray[currTraceHel].axisPoints.size()-1].y)/2;
         traceCenter.z = (helTraceArray[currTraceHel].axisPoints[0].z + helTraceArray[currTraceHel].axisPoints[helTraceArray[currTraceHel].axisPoints.size()-1].z)/2;
        distCent = getDistance(trueCenter, traceCenter);

        if((distCent < minDist))
        {
             helTrueArray[currTrueHel].alignDirectionHels(&helTraceArray[currTraceHel]);
             Dis = helTrueArray[currTrueHel].findDisplacement(&helTraceArray[currTraceHel], stepSize);
             distCent = Dis.crossDisplace;

             if ((distCent < minDist) && (distCent < distanceFromCenter[currTraceHel]))
             {
                if(minDist != 5)
                    distanceFromCenter[matchedHels[currTrueHel]] = 99999;
                matchedHels[currTrueHel] = currTraceHel;
                distanceFromCenter[currTraceHel] = distCent;
                minDist = distCent;
             }
        }
         //cerr << "Done comparing trace " << currTraceHel << endl;
     }

     if (matchedHels[currTrueHel] == 99999)
     {
         cout << "There is no trace detected for true Helix/Sheet " << currTrueHel+1 << endl;
     }
//cerr << "Done matching true helix " << currTrueHel << endl;
 }


///////////////////////////////
// END MATCHING              //
// BEGIN DISTANCE COMPARISON //
///////////////////////////////
Displacement displaced;
double axisLength = 0;
ofstream outFile;
if(outExist == true)
{
    outFile.open(oFile.c_str());
    outFile << "Using axis interpolation stepSize " << stepSize << endl;
    outFile << "Helix#True, Helix#Trace, LengthTrueAxis, LengthDetectedAxis, TwoWayDistance, CrossDisplacement, LengthDisplacement, LengthErrorProportion, FalsePositives, Sensitivity" << endl << endl;
}

/// //////////////////////////
/// Accuracy print out
vector<int> specArray;
for(int i = 0; i < number_of_hel; i++)
    specArray.push_back(0);
if(outExist == true)
    int nonHLXAA = PrintSpecificity(&helTraceArray, &specArray, pdb, outFile);

for (int currentHel = 0; currentHel <  pdb.hlces.size() + pdb.sheets.size(); currentHel++)
{
    if(one == false)
    {
        if(matchedHels[currentHel] > 100 || shortHelix[currentHel] == true)
        {
            if(currentHel < pdb.hlces.size())
                cout << endl << "trueHelix #" << currentHel+1 << endl << "------------------------------------" << endl;
            else
                cout << endl << "trueSheet #" << currentHel+1 << endl << "------------------------------------" << endl;
        }
        else
        {
            if(currentHel < pdb.hlces.size())
                cout << endl << "trueHelix #" << currentHel+1 << " --> traceHelix" << matchedHels[currentHel] << endl << "------------------------------------" << endl;
            else
                cout << endl << "trueSheet #" << currentHel+1 << " --> traceSheet" << matchedHels[currentHel]-offset-1 << endl << "------------------------------------" << endl;
        }
    }
    if(one == true)
    {
        if(matchedHels[currentHel] > 100 || shortHelix[currentHel] == true)
        {
            if(currentHel < pdb.hlces.size())
                cout << endl << "trueHelix #" << currentHel+1 << endl << "------------------------------------" << endl;
            else
                cout << endl << "trueSheet #" << currentHel+1 << endl << "------------------------------------" << endl;

        }
        else
        {
            if(currentHel < pdb.hlces.size())
                cout << endl << "trueHelix #" << currentHel+1 << " --> traceHelix" << matchedHels[currentHel]+1 << endl << "------------------------------------" << endl;
            else
                cout << endl << "trueSheet #" << currentHel+1 << " --> traceSheet" << matchedHels[currentHel]-offset << endl << "------------------------------------" << endl;

        }

    }

    if (matchedHels[currentHel] != 99999 && shortHelix[currentHel] != true)   // If there is a match
        {
            helTrueArray[currentHel].alignDirectionHels(&helTraceArray[matchedHels[currentHel]]);
            displaced = helTrueArray[currentHel].findDisplacement(&helTraceArray[matchedHels[currentHel]], stepSize);
            if(outExist == true)
            {
                outFile << currentHel+1 << ", " << matchedHels[currentHel]+1 << ", ";
                outFile << displaced.lengthAxis1 << ", " << displaced.lengthAxis2 << ", " << displaced.twoWayDistance << ", " << displaced.crossDisplace << ", " << displaced.longDisplace << ", " << displaced.lengthProportion;
                outFile << specArray[matchedHels[currentHel]] << ", ";
                //PrintSensitivity(&helTraceArray[matchedHels[currentHel]], pdb.hlces[currentHel].startIndx, pdb.hlces[currentHel].endIndx, pdb, sradius, outFile);
                outFile << endl;
            }
            cout << "Lateral Discrepancy:\t\t\t" << displaced.crossDisplace << "\nLongitudinal Discrepancy:\t\t      " << displaced.longDisplace << endl;
        }

    else {
        cout << "No trace corresponds enough to true helix/sheet " << currentHel+1 << endl << endl;
        double lengthAxis1 = helTrueArray[currentHel].getArcLength(helTrueArray[currentHel].axisPoints.begin(), helTrueArray[currentHel].axisPoints.end()-1);
        if(outExist == true)
            outFile << currentHel+1 << ", " << "N/A" << ", " << lengthAxis1 << ", " << "N/A" << ", " << "N/A" << ", " << "N/A" << ", " << "N/A" << endl;
    }
}
//cout << "# of nonHLX AA: " << nonHLXAA << endl;
if(outExist == true)
    outFile.close();

return 0;
}
